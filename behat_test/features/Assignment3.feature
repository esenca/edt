Feature: Assignment3: Check if block is ok created on front page

  @api @assign3
  Scenario: Access front page and check block
    Given I am on "/"
    Then I should see "Esenca Drupal test"
    And I should see "Esenca first block"
    And I should see "This block should be placed in the sidebar first region."
    And I should see an "div.esenca-first-block" element
