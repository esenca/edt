<?php

use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
//use Drupal\DrupalExtension\Context\RawDrupalContext;
use NuvoleWeb\Drupal\DrupalExtension\Context\RawDrupalContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;
use Drupal\DrupalExtension\Hook\Scope\BeforeUserCreateScope;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext implements SnippetAcceptingContext {

  protected static $VIDEO_ACCESS_FULL = 0;

  protected static $VIDEO_ACCESS_PER_VIDEO = 1;

  protected static $VOCABULARY_TOKEN_LIMIT = 'annual_access_limit';

  protected static $VOCABULARY_TOKEN_TYPE = 'token_type';

  /**
   * Keep track of all trial tokens entity forms that were created so they can easily be removed.
   *
   * @var array
   */
  protected $trialTokens = array();

  /**
   * Keep track of all access tokens that were created so they can easily be removed.
   *
   * @var array
   */
  protected $accessTokens = array();


  /**
   * Keep track of all users created with tokens so they can easily be removed.
   *
   * @var array
   */
  protected $users = array();


  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
  }

  /** @BeforeScenario */
  public function before(BeforeScenarioScope $scope) {
  }

  /** @AfterScenario */
  public function after(AfterScenarioScope $scope) {
    // Clear all trial tokens created in the scenario.
    foreach ($this->trialTokens as $id => $trialToken) {
      // First delete access token connected to the trial token.
      $accessToken = $trialToken->field_trial_token->value();
      if (!empty($accessToken->nid)) {
        node_delete($accessToken->nid);
      }

      // Then delete trial token (entity form).
      $success = entity_delete('entityform', $id);
      if ($success === FALSE) {
        throw new \Exception(sprintf("Entity token '%s' could not be deleted.", $id));
      }
    }

    // Clear all access tokens created in the scenario.
    foreach ($this->accessTokens as $id => $accessToken) {
      // Delete access token.
      entity_delete('node', $accessToken->getIdentifier());
    }

    // Clear all users created in the scenario.
    // This is no needed if function $this->userCreate($user) is used
    // for user creation.
  }

  /**
   * @AfterStep
   */
  public function takeScreenshotAfterFailedStep($event) {
    if ($event->getTestResult()->getResultCode() === \Behat\Testwork\Tester\Result\TestResult::FAILED) {
      $driver = $this->getSession()->getDriver();
      if ($driver instanceof \Behat\Mink\Driver\Selenium2Driver) {
        $stepText = $event->getStep()->getText();
        $fileName = preg_replace('#[^a-zA-Z0-9\._-]#', '', $stepText) . '.png';

        // Add error tag to image.
        $fileName = 'error-' . $fileName;

        // Get screenshot directory.
        $filePath = $this->getScreenshotPath();

        // Save screenshot.
        $this->saveScreenshot($fileName, $filePath);
        print "Screenshot for '{$stepText}' placed in " . $filePath . DIRECTORY_SEPARATOR . $fileName . "\n";
      }
    }
  }

  /**
   * Call this function before users are created.
   *
   * @beforeUserCreate
   */
  //  public function beforeUserCreateEventCallback(BeforeUserCreateScope $account) {
  //
  //    print_r('----------------- haj da vidimo');
  //
  //
  //    //    $account->field_user_name['und'][0]['value'] = $account->name;
  //    //    $account->field_user_surname['und'][0]['value'] = $account->surname;
  //    //    unset($account->surname);
  //
  //    echo 'ipak ovo radi' . "\n\n\n";
  //    //$account->name .= 'dodosamvovo';
  //  }

  /**
   * Get absolute path to save screenshots.
   *
   * @return string absolute without last / or false if path doesn't exist.
   */
  public function getScreenshotPath() {
    $screenshotPath = '/var/www/html/behat_test/screenshots';

    if ($this->getMinkParameter('files_path')) {
      $screenshotPath = rtrim(realpath($this->getMinkParameter('files_path')), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR. '../screenshots';
    }

    return realpath($screenshotPath);
  }

  /**
   * Save custom screenshot.
   *
   * @Then (I )take the custom screenshot :name
   */
  public function takeCustomScreenshot($name = NULL) {
    $driver = $this->getSession()->getDriver();
    if ($driver instanceof \Behat\Mink\Driver\Selenium2Driver) {
      $fileName = preg_replace('#[^a-zA-Z0-9\._-]#', '', $name) . '.png';
      // Add timestamp to image.
      $fileName = time() . '-' . $fileName;

      // Get screenshot directory.
      $filePath = $this->getScreenshotPath();

      // Save screenshot.
      $this->saveScreenshot($fileName, $filePath);
      print "Screenshot for '{$name}' placed in " . $filePath . DIRECTORY_SEPARATOR . $fileName . "\n";
    }
  }

  /**
   * Creates multiple trail tokens.
   *
   * Provide tokens data in the following format:
   *
   * | video access      | token limit  | expiration date  | organisation | token type |
   * | Full access       | 10           |                  |              |            |
   * | Per video access  | 1            | yyyy-mm-dd hh:mm | Esenca       | club       |
   * | Per video access  | 1            | +30 sec          | Esenca       | business   |
   *
   * @Given /^trial access tokens:$/
   */
  public function createTrialAccessTokens(TableNode $tokensTable) {
    foreach ($tokensTable->getHash() as $tokenHash) {

      // Create entity form.
      $trial_token_form = entity_create('entityform', array(
        'type' => "trial_access_tokens",
        'created' => time(),
        'changed' => time(),
        'language' => LANGUAGE_NONE,
        'uid' => 1,
      ));

      $wrapper = entity_metadata_wrapper('entityform', $trial_token_form);

      /*
       * And set the fields.
       */
      // Set video access field.
      if (!empty($tokenHash['video access'])) {
        // If 'Per video access' then set video, otherwise, set full access.
        if ($tokenHash['video access'] == 'Per video access') {
          $wrapper->field_video_access_trial = $this::$VIDEO_ACCESS_PER_VIDEO;
          // Set field_video
          $wrapper->field_video->set('686'); // here should go node id of video node, NEED TO CHECK THIS!
        }
        elseif ($tokenHash['video access'] == 'Full access') {
          $wrapper->field_video_access_trial = $this::$VIDEO_ACCESS_FULL;
        }
        else {
          $wrapper->field_video_access_trial = $this::$VIDEO_ACCESS_FULL;
        }
      }
      else {
        $wrapper->field_video_access_trial = $this::$VIDEO_ACCESS_FULL;
      }

      // Set token limit.
      if (empty($tokenHash['token limit'])) {
        $tokenHash['token limit'] = '1';
      }
      $token_limit_term = taxonomy_get_term_by_name($tokenHash['token limit'], $this::$VOCABULARY_TOKEN_LIMIT);
      if (!empty($token_limit_term)) {
        $wrapper->field_access_token_limit->set(key($token_limit_term));
      }
      else {
        throw new \Exception(sprintf("The term with name '%s' does not exists.", $tokenHash['token limit']));
      }

      // Set expirationdate if it's set.
      if (!empty($tokenHash['expiration date'])) {
        $exp_date = strtotime($tokenHash['expiration date']);
        if ($exp_date !== FALSE) {
          $wrapper->field_expiration_date->set($exp_date);
        }
      }

      // Set organization if is set.
      if (!empty($tokenHash['organisation'])) {
        $wrapper->field_organisation_trial->set($tokenHash['organisation']);
      }

      // Set token type.
      if (!empty($tokenHash['token type'])) {
        switch ($tokenHash['token type']) {
          case 'business':
          case 'club':
            $token_type_term = taxonomy_get_term_by_name($tokenHash['token type'], $this::$VOCABULARY_TOKEN_TYPE);
            if (!empty($token_type_term)) {
              $wrapper->field_token_type->set(key($token_type_term));
            }
            else {
              throw new \Exception(sprintf("The term with name '%s' does not exists.", $tokenHash['token type']));
            }
            break;

          default:
            $token_type_term = taxonomy_get_term_by_name('club', $this::$VOCABULARY_TOKEN_TYPE);
            if (!empty($token_type_term)) {
              $wrapper->field_token_type->set(key($token_type_term));
            }
            else {
              throw new \Exception(sprintf("The term with name '%s' does not exists.", $tokenHash['token type']));
            }
            break;
        }
      }

      // Save token.
      $trialToken = $wrapper->save();
      if (empty($trialToken)) {
        throw new \Exception(sprintf("The trail token has not be saved."));
      }

      // Print info about entity form and access token.
      echo sprintf("Created Entity form trial token: %s\n", $trialToken->getIdentifier());
      $accessToken = $trialToken->field_trial_token->value();
      echo sprintf("Created Access token: %s (%s)\n", $accessToken->nid, $accessToken->vid);

      $this->trialTokens[$trialToken->getIdentifier()] = $trialToken;
    }

  }

  /**
   * Creates multiple access tokens.
   *
   * Provide tokens data in the following format:
   *
   * | annual access | token limit  | expiration date  |  token type |
   * | 1             | 10           |                  |             |
   * | 1             | 10           |                  |  business   |
   * | 0             | 1            | yyyy-mm-dd hh:mm |  club       |
   *
   * @Given /^access tokens:$/
   */
  public function createAccessTokens(TableNode $tokensTable) {
    foreach ($tokensTable->getHash() as $tokenHash) {

      // Create entity form.
      $trial_token_form = entity_create('node', array(
        'type' => "access_token",
        'created' => time(),
        'changed' => time(),
        'language' => LANGUAGE_NONE,
        'uid' => 1,
      ));

      $wrapper = entity_metadata_wrapper('node', $trial_token_form);

      /*
       * And set the fields.
       */

      // Set annual access.
      if (!empty($tokenHash['annual access'])) {
        $annual_access = intval($tokenHash['annual access']);
        $wrapper->field_annual_access->set($annual_access);
      }
      else {
        $wrapper->field_annual_access->set(1);
      }

      // Set token limit.
      if (empty($tokenHash['token limit'])) {
        $tokenHash['token limit'] = '1';
      }
      $token_limit_term = taxonomy_get_term_by_name($tokenHash['token limit'], $this::$VOCABULARY_TOKEN_LIMIT);
      if (!empty($token_limit_term)) {
        $wrapper->field_access_token_limit->set(key($token_limit_term));
      }
      else {
        throw new \Exception(sprintf("The term with name '%s' does not exists.", $tokenHash['token limit']));
      }

      // Set expirationdate if it's set.
      if (!empty($tokenHash['expiration date'])) {
        $exp_date = strtotime($tokenHash['expiration date']);
        if ($exp_date !== FALSE) {
          $wrapper->field_custom_expiration_date->set($exp_date);
        }
      }

      // Set token type.
      if (!empty($tokenHash['token type'])) {
        switch ($tokenHash['token type']) {
          case 'business':
          case 'club':
            $token_type_term = taxonomy_get_term_by_name($tokenHash['token type'], $this::$VOCABULARY_TOKEN_TYPE);
            if (!empty($token_type_term)) {
              $wrapper->field_token_type->set(key($token_type_term));
            }
            else {
              throw new \Exception(sprintf("The term with name '%s' does not exists.", $tokenHash['token type']));
            }
            break;

          default:
            $token_type_term = taxonomy_get_term_by_name('club', $this::$VOCABULARY_TOKEN_TYPE);
            if (!empty($token_type_term)) {
              $wrapper->field_token_type->set(key($token_type_term));
            }
            else {
              throw new \Exception(sprintf("The term with name '%s' does not exists.", $tokenHash['token type']));
            }
            break;
        }
      }

      // Save token.
      $accessToken = $wrapper->save();
      if (empty($accessToken)) {
        throw new \Exception(sprintf("The access token has not be saved."));
      }

      // Print info about access token.
      echo sprintf("Created Access token: %s (%s)\n", $accessToken->title->value(), $accessToken->getIdentifier());

      $this->accessTokens[$accessToken->getIdentifier()] = $accessToken;
    }
  }

  /**
   * @Then /^trial tokens should exist$/
   */
  public function trialTokensShouldExist() {
    foreach ($this->trialTokens as $id => $token) {
      // Check if entity form exists.
      $form = entity_load_single('entityform', $id);
      if (empty($form)) {
        throw new \Exception(sprintf("The token entity form '%s' does not exists.", $id));
      }

      // Also check if access token (node) exists.
      $node = $token->field_trial_token->value();
      if (empty($node) || $node->type !== 'access_token') {
        throw new \Exception(sprintf("The token does not exists."));
      }
    }
  }

  /**
   * Creates multiple users with tokens.
   *
   * Provide user data in the following format:
   *
   * | name     | mail         | roles        | tokens |
   * | user foo | foo@bar.com  | role1, role2 | random |
   *
   * put 'random' string for random created token
   *
   * @Given /^users with tokens:$/
   */
  public function usersWithTokens(TableNode $usersTable) {
    foreach ($usersTable->getHash() as $userHash) {

      // Split out roles to process after user is created.
      $roles = array();
      if (isset($userHash['roles'])) {
        $roles = explode(',', $userHash['roles']);
        $roles = array_filter(array_map('trim', $roles));
        unset($userHash['roles']);
      }

      $user = (object) $userHash;
      // Set a password.
      if (!isset($user->pass)) {
        $user->pass = $this->getRandom()->name();
      }
      $user = $this->userCreate($user);
      $this->users[] = $user;

      // Assign roles.
      foreach ($roles as $role) {
        $this->getDriver()->userAddRole($user, $role);
      }

      // Check if user need to be created with token.
      if (!empty($userHash['tokens'])) {
        if ($userHash['tokens'] == 'random' &&
          (!empty($this->trialTokens) || !empty($this->accessTokens))) {

          // Check if access token exist or trial token.
          if (!empty($this->trialTokens)) {
            $one_form = reset($this->trialTokens);
            // Pik one of already created tokens.
            $one_token = $one_form->field_trial_token->value();
            $one_token_nid = $one_token->nid;
          }
          else {
            // Pik one of already created tokens.
            $one_token = reset($this->accessTokens);
            $one_token_nid = $one_token->getIdentifier();
          }

          // Load user with all fields.
          $user = user_load($user->uid);
          $user_wrapper = entity_metadata_wrapper('user', $user);
          $tokens_ids = array(
            $one_token_nid,
          );

          $user_wrapper->field_access_token_user->set($tokens_ids);
          $user_wrapper->save();
        }
      }
    }
  }

  /**
   * @Then /^I should have the "([^"]*)" role$/
   */
  public function iShouldHaveTheRole($role) {
    $manager = $this->getUserManager();
    $user = $manager->getCurrentUser();

    $drupal_user = user_load($user->uid);
    $roles = $drupal_user->roles;
    if (!array_search($role, $roles)) {
      throw new \Exception(sprintf("The current user did not have a role of '%s'", $role));
    }
  }

  /**
   * @Then /^I should not have the "([^"]*)" role$/
   */
  public function iShouldNotHaveTheRole($role) {
    $manager = $this->getUserManager();
    $user = $manager->getCurrentUser();

    $drupal_user = user_load($user->uid);
    $roles = $drupal_user->roles;
    if (array_search($role, $roles)) {
      throw new \Exception(sprintf("The current user has the '%s' role", $role));
    }
  }

  /**
   * @Given /^I give myself the "([^"]*)" role$/
   */
  public function iGiveMyselfTheRole($rolename) {
    $manager = $this->getUserManager();
    $user = $manager->getCurrentUser();

    $account = user_load($user->uid);
    $roles = user_roles();
    $rid = array_search($rolename, $roles);
    $userroles = $account->roles;
    $userroles[$rid] = $rolename;
    user_save($account, array('roles' => $userroles));
  }

  /**
   * Delete access token for currently logged in user.
   *
   * If user has multiple access tokens, then delete them all.
   *
   * @When /^my access token has been deleted$/
   */
  public function myAccessTokenHasBeenDeleted() {
    $manager = $this->getUserManager();
    $user = $manager->getCurrentUser();

    $account = user_load($user->uid);

    if (!empty($account->field_access_token_user[LANGUAGE_NONE])) {
      foreach ($account->field_access_token_user[LANGUAGE_NONE] as $index => $access_token) {
        node_delete($access_token['nid']);
      }
    }
  }

  /**
   * Click on the element with the provided xpath query
   *
   * @When /^I click on the element with xpath "([^"]*)"$/
   */
  public function iClickOnTheElementWithXPath($xpath) {
    $session = $this->getSession(); // get the mink session
    $element = $session->getPage()->find(
      'xpath',
      $session->getSelectorsHandler()->selectorToXpath('xpath', $xpath)
    ); // runs the actual query and returns the element

    // errors must not pass silently
    if (NULL === $element) {
      throw new \InvalidArgumentException(sprintf('Could not evaluate XPath: "%s"', $xpath));
    }

    // ok, let's click on it
    $element->click();

  }

  /**
   * Click on the element with the provided CSS Selector
   *
   * @When /^I click on the element with css selector "([^"]*)"$/
   */
  public function iClickOnTheElementWithCSSSelector($cssSelector) {
    $session = $this->getSession();
    $element = $session->getPage()->find(
      'xpath',
      $session->getSelectorsHandler()->selectorToXpath('css', $cssSelector) // just changed xpath to css
    );
    if (NULL === $element) {
      throw new \InvalidArgumentException(sprintf('Could not evaluate CSS Selector: "%s"', $cssSelector));
    }

    $element->click();

  }

  /**
   * @When /^I click on video buy now button$/
   */
  public function iClickOnVideoBuyNowButton() {
    $session = $this->getSession();
    $cssSelector = "#edit-submit";
    $element = $session->getPage()->find(
      'xpath',
      $session->getSelectorsHandler()->selectorToXpath('css', $cssSelector) // just changed xpath to css
    );
    if (NULL === $element) {
      throw new \InvalidArgumentException(sprintf('Could not evaluate CSS Selector: "%s"', $cssSelector));
    }

    if (!$element->isVisible()) {
      $locator = 'add-to-cart-token';
      $this->getSession()->executeScript("
                var element = document.getElementsByClassName('".$locator."');
                element[0].style.display = 'block';
            ");
      $this->getSession()->wait(5000);

      $locator = 'edit-submit';
      $this->getSession()->executeScript("
                var element = document.getElementById('".$locator."');
                element.style.display = 'block';
            ");
      $this->getSession()->wait(5000);

      $element = $session->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath('css', "#edit-submit") // just changed xpath to css
      );
    }

    if ($element->isVisible()) {
      $element->click();

    }
    else {
      throw new \Exception(sprintf("Video buy now button is not visible."));
    }

    // Submit the form explicitly, otherwise it doesn't work.
    $cssSelector = "form.commerce-add-to-cart";
    $element = $session->getPage()->find(
      'xpath',
      $session->getSelectorsHandler()->selectorToXpath('css', $cssSelector) // just changed xpath to css
    );
    if (NULL === $element) {
      throw new \InvalidArgumentException(sprintf('Could not evaluate CSS Selector: "%s"', $cssSelector));
    }
    $element->submit();

  }
}
