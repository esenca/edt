<?php

use NuvoleWeb\Drupal\DrupalExtension\Context\EmailContext;

use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;
use Drupal\DrupalExtension\Hook\Scope\BeforeUserCreateScope;
use function bovigo\assert\assert;
use function bovigo\assert\predicate\hasKey;
use function bovigo\assert\predicate\contains;
use function bovigo\assert\predicate\matches;

class CustomEmailContext extends EmailContext {

  /**
   * Check if mail is sent to every mail in the list, in appropriate order.
   *
   * | email_1 | somemail@mail.com  |
   * | email_2 | somemail2@mail.com |
   * | ...     | ...                |
   * | email_i | somemaili@mail.com |
   *
   * @Then an email should be sent to list:
   */
  public function anEmailShouldBeSentToList(TableNode $table) {
    $emails = $this->getCollectedEmails();
    if (!$emails) {
      throw new \Exception('No mail was sent.');
    }

    // Put each mail address into one array.
    $emails_to = array();
    foreach ($emails as $index => $email) {
      $emails_to[$index] = trim($email['to']);
    }

    // Print debug messages.
    echo "----- debug -----\n";
    echo "mail sent to the following adresses:\n";
    print_r($emails_to);

    // Get rows hash.
    $rows_hash = $table->getRowsHash();

    // Check if count of mails match.
    if (count($rows_hash) != count($emails_to)) {
      throw new \Exception('Mail should be sent to ' . count($rows_hash) . ' email, but it sent to ' . count($emails_to) . ' emails.');
    }

    // Check foreach needle if mail is sent.
    foreach ($rows_hash as $name => $value) {
      if (!in_array($value, $emails_to)) {
        throw new \Exception('No mail was sent to ' . $value);
      }
    }
  }

  /**
   * Assert that the email that has been sent has the given params.
   *
   * This function use contains() method, so you can use partial match.
   * Following params can be used (all are optional)
   * | subject  | Some subject                                        |
   * | message  | Some content in message, you can use html tags here |
   * | langcode | en                                                  |
   *
   * @Then an email should containing following params:
   */
  public function assertEmailContainingParams(TableNode $table) {
    $last_mail = $this->getLastEmail();
    $mail_params = $last_mail['params'];
    foreach ($table->getRowsHash() as $name => $value) {
      assert($mail_params, hasKey($name));
      assert($mail_params[$name], contains($value));
    }
  }

  /**
   * Assert that the email that has been sent has the given params.
   *
   * You need to use regular expression here, in format adjusted for preg_match() function.
   * Following params can be used (all are optional)
   * | subject  | Some subject                                        |
   * | message  | Some content in message, you can use html tags here |
   * | langcode | en                                                  |
   *
   * @Then an email should containing following params as regex:
   */
  public function assertEmailContainingParamsAsRegex(TableNode $table) {
    $last_mail = $this->getLastEmail();
    $mail_params = $last_mail['params'];
    foreach ($table->getRowsHash() as $name => $value) {
      assert($mail_params, hasKey($name));
      assert($mail_params[$name], matches($value));
    }
  }
}