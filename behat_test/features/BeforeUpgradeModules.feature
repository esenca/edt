Feature: Assignment1: Check if site need security upgrade.

  @api @javascript @beforeUpgradeModules
  Scenario: Access front page
    Given I am on "/"
    Then I should see "Esenca Drupal test"
    And I should see " This site is for Esenca assignments testing"
    And take the custom screenshot "before-upgrade-modules-homepage"

  @api @javascript @beforeUpgradeModules
  Scenario: Access report page and check that Drupal need security upgrade.
    Given users:
      | name                | mail                | pass     | roles         |
      | drupal@esencate.com | drupal@esencate.com | gI#gV9&F | administrator |
    And I am logged in as "drupal@esencate.com"
    And I am on "/admin/reports/status"
    Then I should see "Module and theme update status"
    And I should see "There are security updates available for one or more of your modules or themes. To ensure the security of your server, you should update immediately!"
    And take the custom screenshot "before-upgrade-modules-status"
