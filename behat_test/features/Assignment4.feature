Feature: Assignment4: Check if views with News has been created successfully.

  @api @assign4
  Scenario: Access News page and check news' list.
    Given I am logged in as a user with the '<em class="placeholder">News</em>: Create new content' permissions
    And I create News content:
      | Title       | Body                    | Source |
      | News 1h8vg  | This is my first news   | CNN    |
      | News 2mb9s  | This is my second news  | CNN    |
      | News 3bgo29 | This is my third news   | CNN    |
      | News 4gb2h  | This is my forth news   | CNN    |
      | News 5b1is  | This is my fifth news   | CNN    |
      | News 6gbow  | This is my sixth news   | CNN    |
      | News 7b219  | This is my seventh news | BBC    |
      | News 8dfdfd | This is my eighth news  | BBC    |
      | News 9awnkd | This is my nineth news  | BBC    |
      | News 10d9fa | This is my tenth news   | BBC    |
    And I am on "/"
    When I click "News"
    Then I should see "News 10d9fa"
    And I should see "News 6gbow"
    And I should not see "News 5b1is"
    When I click "2"
    Then I should see "News 1h8vg"
    And I should see "News 5b1is"
    And I should not see "News 6gbow"
    When I fill in "field_source_value" with "BBC"
    And I press the "Apply" button
    Then I should see "News 7b219"
    And I should see "News 8dfdfd"
    And I should see "News 9awnkd"
    And I should see "News 10d9fa"
    And I should not see "News 1h8vg"
    And I should not see "News 6gbow"
