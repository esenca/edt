Feature: Assignment1: Check if docker, site and test are good configured.

  @api @assign1
  Scenario: Access front page
    Given I am on "/"
    Then I should see "Esenca Drupal test"
    And I should see " This site is for Esenca assignments testing"

  @api @assign1
  Scenario: Access front page as login user
    Given users:
      | name                | mail                | pass     |
      | drupal@esencate.com | drupal@esencate.com | gI#gV9&F |
    And I am logged in as "drupal@esencate.com"
    And I am on "/"
    Then I should see "My account"
    And I should see "Log out"
