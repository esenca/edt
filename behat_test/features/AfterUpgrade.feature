Feature: Assignment1: Check if site has bees successfully upgraded.

  @api @javascript @afterUpgrade
  Scenario: Access front page
    Given I am on "/"
    Then I should see "Esenca Drupal test"
    And I should see " This site is for Esenca assignments testing"
    And take the custom screenshot "after-upgrade-homepage"

  @api @javascript @afterUpgrade
  Scenario: Access report page and check that Drupal need security upgrade.
    Given users:
      | name                | mail                | pass     | roles         |
      | drupal@esencate.com | drupal@esencate.com | gI#gV9&F | administrator |
    And I am logged in as "drupal@esencate.com"
    And I am on "/admin/reports/status"
    Then I should see "Drupal core update status"
    And I should not see "There is a security update available for your version of Drupal. To ensure the security of your server, you should update immediately!"
    And take the custom screenshot "after-upgrade-status"
