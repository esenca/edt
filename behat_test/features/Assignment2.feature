Feature: Assignment2: Check if developer learn how to create Drupal features.

  @api @assign2
  Scenario: Access front page and check slogan
    Given I am on "/"
    Then I should see "Esenca Drupal test"
    And I should see " This site is for Esenca assignments testing and it works."
