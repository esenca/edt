# README #

This README use markdown notation, for more info check [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Esenca Drupal test project ###

* Drupal site with Netbeans and Docker support
* Version 1.2

### Basic informations ###

* As first step, make fork of this repository
    * more info on [link](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)
    * and detailed information of complete process on [link](https://www.atlassian.com/git/tutorials/learn-about-code-review-in-bitbucket-cloud)
* all assignments should be pushed on forked repository
* at the end, when all assignments are resolved and all tests passed, create a pull request from the forked repository
 (source) back to the original (destination) 
    
* test site with be access locally at address http://edt.ddev.local if you setup system with ddev as described bellow, or on on http://edt.docker.localhost:8000/ if you use some other way.
* admin account:
```
user: esenca
pass: esenca123
```
* assignments for the test are in the section [Assignments](#assignments) in this document

### Set up environment ###
* Clone project from this git repo
* Project contains PhpStorm (IDEA) and Netbeans' configuration files, so you can just open with PhpStorm (IDEA)-recomended or Netbeans.
* You need to deploy project With DDev (Recommended), Docker or Manually as described bellow

#### With DDev (Recommended) ####

* With DDev, your environment will be set up automatically. Otherwise, check manual installation section bellow.
* Documentation link https://ddev.readthedocs.io/en/stable/
* After installing DDev on local computer, you can run project with following commands
```bash
ddve start
```
* First you need to import files folder with command
```bash
ddev import-files --src=.esenca/files
```
* Than you need to import initial database from .sql file with this command from project root directory
```bash
ddev import-db --src=docker/mariadb-init/drupal.sql.gz
```
* And then you can access project via url http://edt.ddev.local/
* When you finish work on project you can remove project (preserving database) with command
```bash
ddev remove
```
* If you want to remove project and database run this command (this will create database snapshot)
```bash
ddev remove --remove-data
```
or without snapshot
```bash
ddev remove --remove-data --omit-snapshot
```

#### With Docker ####

* With docker, your environment will be set up automaticly. Otherwise, check manual installation section bellow.
* Install docker and docker-compose
    * For Ubuntu check url https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04
    * and url for composer https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-16-04
    * For Windows check url https://www.docker.com/docker-windows (Requires Microsoft Windows 10 Professional or Enterprise 64-bit)
    * for older windows version check https://www.docker.com/products/docker-toolbox
* Add following into /etc/hosts file (Linux), for Windows check http://www.thewindowsclub.com/hosts-file-in-windows
```
127.0.0.1       pma.drupal.docker.localhost
127.0.0.1       adminer.drupal.docker.localhost
127.0.0.1       mailhog.drupal.docker.localhost
127.0.0.1       solr.drupal.docker.localhost
127.0.0.1       nodejs.drupal.docker.localhost
127.0.0.1       front.drupal.docker.localhost
127.0.0.1       varnish.drupal.docker.localhost
127.0.0.1       portainer.drupal.docker.localhost
127.0.0.1       edt.docker.localhost
```
* Run the following script (for Linux and Mac)
```
./esenca/deploy-files-locally.sh
```
* For Windows run following script
```
./esenca/deploy-files-locally.bat
```
* Open terminal and go to the project root folder (folder wiht docker-compose.yml file)
* Start docker first time via command
```
docker-compose up -d
```
* Now you can access site from url
```
edt.docker.localhost:8000
```
* Stop docker with command
```
docker-compose stop
```
* Any other time you can start docker with command
```
docker-compose start
```

NOTICE: You can completely turn off docker container with command
```
docker-compose down
```
but in this case you will lose all changes made in docker container. If you haven't made any changes
in any docker container, you can safely turn off containers. Database will be preserved, it is stored
externally on location .esenca/databse/raw

* To set file permissions on Linux just run following commands, detailed instructions on https://docker4drupal.readthedocs.io/en/latest/permissions/ (run them from root project folder):
```
sudo setfacl -dR -m u:$(whoami):rwX -m u:82:rwX -m u:100:rX public_html;
sudo setfacl -R -m u:$(whoami):rwX -m u:82:rwX -m u:100:rX public_html;

sudo setfacl -dR -m u:$(whoami):rwX -m u:100:rwX -m g:101:rwX .esenca/database/raw;
sudo setfacl -R -m u:$(whoami):rwX -m u:100:rwX -m g:101:rwX .esenca/database/raw;

sudo setfacl -dR -m u:$(whoami):rwX -m u:82:rwX -m g:100:rwX .esenca/patches;
sudo setfacl -R -m u:$(whoami):rwX -m u:82:rwX -m g:100:rwX .esenca/patches;

sudo setfacl -dR -m u:$(whoami):rwX -m u:82:rwX -m u:100:rwX public_html/sites/default/files;
sudo setfacl -R -m u:$(whoami):rwX -m u:82:rwX -m u:100:rwX public_html/sites/default/files;

```

#### Manual installation ####

* First you need to have install some PHP environment, for example LAMP or XAMP or WAMP
* Create Apache virtual host and point root directory to public_html dir in project folder.
Here's setting for virtul host, just replace <absolute-project-directory-location> for actual location

```
<VirtualHost *:80>
        # The ServerName directive sets the request scheme, hostname and port that
        # the server uses to identify itself. This is used when creating
        # redirection URLs. In the context of virtual hosts, the ServerName
        # specifies what hostname must appear in the request's Host: header to
        # match this virtual host. For the default virtual host (this file) this
        # value is not decisive as it is used as a last resort host regardless.
        # However, you must set it for any further virtual host explicitly.
        #ServerName www.example.com

        ServerAdmin webmaster@localhost
        DocumentRoot <absolute-project-directory-location>/public_html

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        # For most configuration files from conf-available/, which are
        # enabled or disabled at a global level, it is possible to
        # include a line for only one particular virtual host. For example the
        # following line enables the CGI configuration for this host only
        # after it has been globally disabled with "a2disconf".
        #Include conf-available/serve-cgi-bin.conf

        # Directory settings.
        ServerName edt.docker.localhost
        <Directory <absolute-project-directory-location>/public_html/>
          Options +Indexes +FollowSymLinks +MultiViews +Includes
          AllowOverride All
          Order allow,deny
          allow from all
        </Directory>
</VirtualHost>
```

* Add following into /etc/hosts file (Linux), for Windows check http://www.thewindowsclub.com/hosts-file-in-windows
```
127.0.0.1       edt.localhost
```
* Create database and import initial version from directory <PROJECT-ROOT>/docker/mariadb-init/
* Copy "files" folder from "./esenca/files" into "./public_html/sites/default/"
* Copy "settings.php" file from "./esenca/" into "./public_html/sites/default/"
* Change your Drupal settings.php file to use your database
* Now you can access site from url
```
edt.localhost
```
* Clean Drupal cache and you can start using site


### Contribution guidelines ###

* Netbeans' configuration for this project contains formatting settings. For good code formating 
just press Alt+Shift+F shortcut or right mouse click on document and click on "Format" item.
You can find detailed instructions on https://www.drupal.org/docs/develop/development-tools/configuring-netbeans
* Create new Git flow feature for any task (https://www.atlassian.com/git/tutorials/comparing-workflows)
* When finish the work complete git feature (always use rebase, not merge). Everything should be rebased and merged into develop branch.
* Push develop branch to Bitbucket.
* Changes will be automatically applied to test server.

#### Export configuration ####
If you need to export some configurations to git, you need to use Features module.
More info about that:

* Drupal's module [Featrues Module](https://www.drupal.org/project/features)
* Instruction [How to use](https://www.slideshare.net/svilensabev/building-and-deployment-of-drupal-sites-with-features-and-context-6696247)
* And [video](https://www.youtube.com/watch?v=MoMS8Z3Wp2o)

#### Run tests ####
To run tests, you need to login to ddev web container with following command
 (command need to be run from project root directory)
```bash
ddev ssh
```
then go to the folder /var/www/html/behat_test/
```bash
cd /var/www/html/sites/all/behat_test
```
and run tests with command (this will run tests for first assignment; change number for other assignments)
```bash
./bin/behat --tags '@assign1'
```

#### Run tests on Bitbucket pipelines
* First you need to enable Bitbucket pipelines on your Bitbucket cloud
    * https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html
    * bitbucket-pipelines.yml file is already created, so you need just to click on Enable button
* After that you can run test for each assignment
    * https://confluence.atlassian.com/bitbucket/run-pipelines-manually-861242583.html
* IMPORTANT: If you use Bitbucket free account, you only have 50 free minutes pre month for pipelines builds

### Who do I talk to? ###

* For more information contact sasa.jovanovic@esenca.rs

## Assignments ##

### Assignment 1 ###
* Setup test site in local environment as described above
* Run tests for first assignment (all test must pass)
```bash
./bin/behat --tags '@assign1'
```
* Run test 'custom:assign1' via Bitbucket pipelines (procedure has been described above)

### Assignment 2 ###
* Create new git branch from master. New branch name: assign2
* All changes in this assignment should be committed to the 'assign2' branch
* Change site slogan in following:
```
This site is for Esenca assignments testing and it works.
```
* Create Drupal's features that handle this change and commit (there is instructions above)
    * Feature name should be: feature_assign1, and should be placed in public_html/sites/all/modules/custom_features folder.
* Run tests
```bash
./bin/behat --tags '@assign2'
```
* Merge branch assign2 into master branch.
* Push everything on Bitbucket
* Run test 'custom:assign2' via Bitbucket pipelines (procedure has been described above)

### Assignment 3 ###
* Create new git branch from master. New branch name: assign3
* All changes in this assignment should be committed to the 'assign3' branch
* Create custom Drupal's block with following data:

Data              | Value
-------------     | -------------
Machine name      | esenca_first_block
Block title       | Esenca first block
Block description | First block description
Block body        | This block should be placed in the sidebar first region.

* Add following css class to the block (useful [link](https://www.youtube.com/watch?v=SP2KucLtBqI))
```
esenca-first-block
```
* Create Drupal's features that handle this change and commit (there is instructions above)
    * Feature name should be: feature_assign3, and should be placed in public_html/sites/all/modules/custom_features folder.
* Run tests
```bash
./bin/behat --tags '@assign3'
```
* Merge branch assign3 into master branch.
* Push everything on Bitbucket
* Run test 'custom:assign3' via Bitbucket pipelines (procedure has been described above)

### Assignment 4 ###
* Create new git branch from master. New branch name: assign4
* All changes in this assignment should be committed to the 'assign4' branch
* Create new content type News (machine name: news)
* It should contains default fields and also one text field called Source
* Create new user that has premissions: 
    * News: Create new content
* Create 10 news with this user (for title or body you can use custom text, for source put CNN or BBC)
* Create new page (called News) with Drupal view that preview news in table (5 news per page).
    * More infor about views on playlist https://www.youtube.com/watch?v=Jfq9Aq9aeyM&index=25&list=PL15BE2E8313A4E809
    * News should be previewed as table with fields: Title, Source
    * Newest news should be list first (sort items per Content nid, descending)
    * Link to this page should be visible in Main menu.
    * This page should only preview news created by currently logged in user.
        * https://www.youtube.com/watch?v=MdJfn2eB63w&list=PL15BE2E8313A4E809&index=36
* Add Exposed filters into view for Source filed, don't enable AJAX for Exposed filters
    * https://www.youtube.com/watch?v=2Yk6rre3vi8
* Here example of page
![News page screenshot](https://bitbucket.org/esenca/edt/raw/master/images/assign4.png)

* Create Drupal's features that handle this change and commit (there is instructions above)
    * Feature name should be: feature_assign4, and should be placed in public_html/sites/all/modules/custom_features folder.
* Run tests
```bash
./bin/behat --tags '@assign4'
```
* Merge branch assign4 into master branch.
* Push everything on Bitbucket
* Run test 'custom:assign4' via Bitbucket pipelines (procedure has been described above)
