#! /bin/bash

cp settings.php ../public_html/sites/default/
cp -vr ./files ../public_html/sites/default/;
echo "";
echo "Files folder successfully overwritten by new files.";

# Set appropriate files permissions.
#setfacl -dR -m u:$(whoami):rwX -m u:82:rwX -m u:100:rwX ../public_html/sites/default/files;
#setfacl -R -m u:$(whoami):rwX -m u:82:rwX -m u:100:rwX ../public_html/sites/default/files;
find ../public_html/sites/default/files -type d -exec chmod u=rwx,g=rwx,o=rwx '{}' \;
find ../public_html/sites/default/files -type f -exec chmod u=rw,g=rw,o=rw '{}' \;
echo "Permission successfully changed.";
