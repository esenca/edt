; Required attributes
api = 2
core = 7.x

; Core patches.
projects[drupal][patch][] = "core/htaccess-fix-security-errors.patch"
; projects[drupal][patch][] = "core/htaccess-custom.patch"
projects[drupal][patch][] = "core/htaccess-files-fix-security-errors.patch"

; Contrib patches.
projects[coffee][patch][] = "coffee_shortcut-configuration.patch"
projects[path_breadcrumbs][patch][] = "https://www.drupal.org/files/issues/2018-04-26/path_breadcrumbs-add_context_to_keyword_substitude-2660378-4.patch"
