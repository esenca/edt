#! /bin/bash
# Delete local database, import new database from docker/mariadb-init/drupal.sql.gz

# Database name
DATABASE_FILE="/docker-entrypoint-initdb.d/drupal.sql.gz"
USERNAME="drupal"
DATABASE_NAME="drupal"
DATABASE_PASS="drupal"

# Turn on maintenance mode.
echo "Start turning on Maintenance mode..."
docker-compose exec --user 82 php bash -c "drush vset maintenance_mode 1;drush cc all"
echo "--- Maintenance mode successfully turned on"

# Backup database
echo "Start backup database $DATABASE_FILE..."
COMMAND="mysqldump -u $USERNAME -p$DATABASE_PASS -h localhost $DATABASE_NAME | gzip -9 > $DATABASE_FILE"
docker-compose exec mariadb bash -c "$COMMAND"
echo "--- Database successfully backed up"

# Turn off maintenance mode.
echo "Start turning off Maintenance mode..."
docker-compose exec --user 82 php bash -c "drush vset maintenance_mode 0;drush cc all"
echo "--- Maintenance mode successfully turned off"
