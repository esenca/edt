#! /bin/bash
# Check if there is some changes in Drupal features.

# Delete all tables from database.
COMMAND="drush -y fda"

echo "Check if there is some changes in Drupal features..."
docker-compose exec --user 82 php bash -c "$COMMAND"
echo  "--- Check completed."
